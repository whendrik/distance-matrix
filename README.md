# Distance Matrix & Other Common Geo Computations

## No correct ways...

https://stackoverflow.com/questions/7222382/get-lat-long-given-current-point-distance-and-bearing/48485675#48485675


Then the main models are:

- Euclidian/Flat earth model: good for very short distances under about 10 km
- Spherical model: good for large longitudinal distances, but with small latitudinal difference. Popular model:
	* Haversine: meter accuracy on [km] scales, very simple code.
- Ellipsoidal models: Most accurate at any lat/lon and distance, but is still a numerical approximation that depend on what accuracy you need. Some popular models are:
	* Lambert: about 10 meter precision over 1000's of km.
	* Paul D.Thomas: Andoyer-Lambert approximation
	* Vincenty: millimeter precision and computational efficiency (`geopy`)
	* Karney: nanometer precision (`pyproj`)

---

Example Code if you need to calculate distances to points-of-interest in python.

Using the haversine distance.

Packages like [haversine](https://pypi.org/project/haversine/) exists, which can quickly calculate the distance pairwise.

## `haversine` for lat/long

When you have lat/long pairs - you wan't to use the [haversine](https://en.wikipedia.org/wiki/Haversine_formula) distance.

The distance metric in `sklearn` has a fairly fast implementation, which can be used like

```
from sklearn.neighbors import DistanceMetric

dist = DistanceMetric.get_metric('haversine')
```

Say `places_gps` and `metro_gps` contain lat/long pairs, we can get the distance in `KM` with

```
EARTH_RADIUS = 6371.009

haversine_distances = dist.pairwise(np.radians(places_gps), np.radians(metro_gps) )
haversine_distances *= EARTH_RADIUS
```

For more detailed examples see the notebook.

## Alternative Method

https://automating-gis-processes.github.io/site/notebooks/L3/nearest-neighbor-faster.html

## Within Radius Example 

Using the https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.BallTree.html#sklearn.neighbors.BallTree.query_radius we can also quickly calculate all places within certain radius.

## pyproj

https://stackoverflow.com/questions/69520272/how-to-calculate-all-points-latitude-and-longitude-between-2-points-every-10-k/69689434#69689434

## 2D Euclidian Distances with FAISS

https://github.com/facebookresearch/faiss

(quite fast - quite scalable)

---

https://iliauk.wordpress.com/2016/02/16/millions-of-distances-high-performance-python/


## Radius at different lat

```

latitude = 90
earthRadiusInMetersAtSeaLevel = 6378137.0
earthRadiusInMetersAtPole = 6356752.314

r1 = earthRadiusInMetersAtSeaLevel
r2 = earthRadiusInMetersAtPole
beta = np.radians(latitude)


this = (r1**2 * cos(beta))**2 + (r2**2 * sin(beta))**2
that = (r1 * np.cos(beta))**2 + (r2 * np.sin(beta))**2

earthRadiuseAtGivenLatitude = (this/that)**.5
```

## On Clustering

https://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/

## Robin

https://gis.stackexchange.com/questions/110583/pyproj-converting-wgs84-to-robinson
